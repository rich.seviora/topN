require 'rspec'

describe TopN::LineReader do
  describe '.new' do
    context 'Valid Settings' do
      subject { described_class.new 'spec/data/numbers.txt' }
      it 'assigns path provided to #file_path' do
        is_expected.to have_attributes file_path: 'spec/data/numbers.txt'
      end
      it 'assigns a IO object to  #stream' do
        expect(subject.stream).to be_a File
      end
    end
  end
end