require 'rspec'
require './lib/top_n'

describe TopN::ValueManager do
  let(:number_limit) { 200 }
  subject { described_class.new(number_limit) }
  describe '.new' do
    it 'assigns number limit correctly' do
      is_expected.to have_attributes number_limit: number_limit
    end
    it 'has empty #top_values upon creation' do
      is_expected.to have_attributes top_values: []
    end
  end
  describe '#process_value' do
    subject {described_class.new(number_limit, :decimal)}
    let(:number_limit) { 10 }
    context 'Within Limits' do
      let(:insert_values) { %w(10.1 0 0.1 -0.1 25 25.2222 123 1000 1 -0.000001) }
      let(:expected_values) { insert_values.map { |v| BigDecimal.new(v) }.sort.reverse }
      it 'stores the values in the array in the correct order' do
        insert_values.each { |value| subject.process_value value }
        result = subject.top_values
        expect(result).to match(expected_values)
      end
    end
    context 'Above Limit' do
      let(:insert_values) do
        array = %w(10.1 9.9)
        10.times { |x| array << '10' }
        array
      end
      let(:expected_values) do
        array = ['10.1']
        9.times { array << '10' }
        array.map { |v| BigDecimal.new(v) }
      end
      it 'stores the values in the array in the correct order' do
        insert_values.each { |v| subject.process_value v }
        expect(subject.top_values).to match(expected_values)
      end
    end
    context 'Far Beyond Limit' do
      let(:insert_values) do
        Array.new(200000) { Random.srand }
      end
      let(:expected_values) { insert_values.map { |v| BigDecimal.new(v) }.sort.reverse.first(10) }
      it 'stores the values in the array in the correct order' do
        insert_values.each { |v| subject.process_value v }
        expect(subject.top_values).to match(expected_values)
      end
    end
    context 'Random Scenarios' do
      subject {described_class.new(number_limit, :integer)}
      [10, 100, 1000, 10000].each do |number_limit|
        [10, 100, 1000, 10000, 100000].each do |sample_size|
          it "handles N=#{number_limit}, Input Size=#{sample_size}" do
            # Arrange
            manager = described_class.new(number_limit)
            insert_values = Array.new(sample_size) { Random.srand }
            expected_values = insert_values.sort.reverse.first(number_limit)
            # Act
            insert_values.each {|v| manager.process_value v}
            # Assert
            expect(manager.top_values).to match(expected_values)
          end
        end
      end
    end
  end
end