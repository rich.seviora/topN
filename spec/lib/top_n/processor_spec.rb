require 'rspec'

describe TopN::Processor do
  let(:reader) { instance_double TopN::LineReader }
  let(:manager) { instance_double TopN::ValueManager }
  describe '.new' do
    context 'Valid Settings' do
      before do
        expect(TopN::LineReader).to receive(:new).with('kitty').and_return(reader)
        expect(TopN::ValueManager).to receive(:new).with(500).and_return(manager)
      end
      subject { described_class.new '500', 'kitty' }
      it 'converts string number limits to integers' do
        expect(subject).to have_attributes number_limit: 500
      end
      it 'assigns path argument to #file_path' do
        expect(subject).to have_attributes file_path: 'kitty'
      end
      it 'assigns a LineReader to #reader' do
        expect(subject).to have_attributes reader: reader
      end
      it 'assigns a ValueManager to #value_manager' do
        expect(subject).to have_attributes value_manager: manager
      end
    end
  end
  describe '#process' do
    let(:answer) { instance_double Array }
    before do
      allow(TopN::LineReader).to receive(:new).and_return(reader)
      allow(TopN::ValueManager).to receive(:new).and_return(manager)
      # Last value should end loop, which is why there's four calls here.
      allow(reader).to receive(:eof?).and_return(false, false, false, true)
      # And only three return values here.
      allow(reader).to receive(:read_next_line).and_return('25.5', '356.12', '259.12')
      allow(manager).to receive(:process_value)
      allow(manager).to receive(:top_values).and_return(answer)
    end
    subject { described_class.new '500', 'kitty' }
    it 'calls value_manager#read_next_line three times' do
      subject.process
      expect(reader).to have_received(:read_next_line).exactly(3).times
    end
    it 'calls value_manager#eof? four times' do
      subject.process
      expect(reader).to have_received(:eof?).exactly(4).times
    end
    it 'returns the top values provided by value_manager#top_values' do
      result = subject.process
      expect(result).to equal(answer)
    end
  end
end