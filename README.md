# topN

This gem processes a file with (whole) numbers on each line and outputs the top _n_ values.

## Design Assumptions

* The current design assumes all values are integers, however decimal support is built-in.
* The top _n_ values will fit into available memory.
* The file to read is local.

## Usage
### Processing a File
To run the program, run:

    bin/process [number_limit] [file_path]

Where:
 
* `number_limit` is the number of top values to retain.
* `file_path` is the path to the file to read.

### Generating a Sample File
To generate a file, run:

    bin/generate [number_count] [file_path]
    
Where:

* `number_count` is the number of values to create.
* `file_path` is the path and name of the file to create.
    
**Note:** If the commands return a `permission denied` error, run the following to grant the correct permissions: 

    chmod 744 bin/process
    chmod 744 bin/generate
    
### Performance
Performance metrics can be generated to evaluate the top N algorithim processing time.

#### Benchmarking
Execute the following to see a comparison of processing performance for a combination of input sizes, values to retain, and performance between decimal and integer inputs:

    bundle exec rake top_n:benchmark

#### Profiling
Execute the following to profile performance for two different input sizes:

    bundle exec rake top_n:profile

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

