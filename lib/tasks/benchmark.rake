require 'benchmark'
require 'ruby-prof'
require './lib/top_n'

namespace :top_n do
  desc 'Benchmark Value Manager Performance'
  task :benchmark do
    number_limits = [10, 100, 1000, 10000]
    input_sizes   = [10000, 100000]
    modes         = [:decimal, :integer]
    Benchmark.bm(35) do |x|
      modes.each do |mode|
        input_sizes.each do |input_size|
          number_limits.each do |number_limit|
            value_manager = TopN::ValueManager.new(number_limit, mode)
            values        = Array.new(input_size) { Random.srand }
            x.report("Mode=#{mode}, N=#{number_limit}, Input=#{input_size}") do
              values.each { |v| value_manager.process_value(v) }
            end
          end
        end
      end
    end
  end

  desc 'Profile value manager performance across multiple number limits.'
  task :profile do
    number_limits = [10, 1000]
    input_sizes   = [10000]
    input_sizes.each do |input_size|
      number_limits.each do |number_limit|
        puts "Profiling N=#{number_limit} with Input Size=#{input_size}"
        value_manager = TopN::ValueManager.new(number_limit, :integer)
        values        = Array.new(input_size) { Random.srand }
        result        = RubyProf.profile do
          values.each { |v| value_manager.process_value(v) }
        end
        printer       = RubyProf::FlatPrinter.new(result)
        printer.print(STDOUT, min_percent: 2)
      end
    end
  end
end