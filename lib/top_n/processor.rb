module TopN
  # The Processor class is responsible for managing the overall process.
  # == To Use
  #
  # 1. Create the processor with valid <tt>number_limit</tt> and <tt>input_file_path</tt> arguments.
  # 2. Call +#process+ to process the file and return the top values.
  #
  class Processor

    # @return [Integer] Number of values to retain.
    attr_reader :number_limit
    # @return [String] Location of input file.
    attr_reader :file_path
    # @return [TopN::LineReader] The reader object utilized.
    attr_reader :reader
    # @return [TopN::ValueManager] The value manager object.
    attr_reader :value_manager

    # @param [#to_i] number_limit - Count of top values to return. Must be at least 1.
    # Decimals/fractionals are truncated per it's #to_i method.
    # @param [String] input_file_path - Path for the input file.
    def initialize(number_limit, input_file_path)
      @number_limit = number_limit.to_i
      @file_path = input_file_path
      validate_settings
      start_components
    end

    # Processes the file and returns an array containing the top values.
    # @return [Array<BigDecimal>] Returns a sorted array of the top values.
    def process
      until reader.eof?
        new_value = reader.read_next_line
        value_manager.process_value new_value
      end
      value_manager.top_values
    end

    private

    # Initializes components for the process manager.
    def start_components
      @reader = TopN::LineReader.new(file_path)
      @value_manager = TopN::ValueManager.new(number_limit)
    end

    # Validates the provided settings.
    def validate_settings
      raise ArgumentError, 'Missing File Path' unless file_path.is_a? String
      unless number_limit >= 1
        raise ArgumentError, "Invalid Number Limit: #{number_limit}, Value must be at least 1."
      end
    end
  end
end
