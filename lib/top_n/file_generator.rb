module TopN
  # The FileGenerator class is responsible for generating test files for use.
  class FileGenerator
    # Initializes the file generator.
    # @param [Integer] number_count - Count of numbers to generate.
    # @param [String] file_location - Location of file to output to.
    def initialize(number_count, file_location)
      @file_location = file_location
      @number_count = number_count
    end

    # @return [String]
    attr_reader :file_location

    # @return [Integer]
    attr_reader :number_count

    # Instructs the generator to create the file.
    def create
      stream = File.open(file_location, 'w')
      number_count.times do
        stream.puts Random.srand
      end
      stream.close
    end
  end
end
