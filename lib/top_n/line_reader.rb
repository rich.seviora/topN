module TopN
  # The LineReader class is a thin wrapper around the Ruby filesystem class.
  class LineReader

    # @return [String] The path of the file to read.
    attr_reader :file_path

    # @return [File] The filestream object.
    attr_reader :stream

    # Initializes the line reader.
    # @param [String] file_path The path of the file to read. Consumer is responsible for providing a valid path.
    def initialize(file_path)
      @file_path = file_path
      @stream = File.open(file_path, 'rt')
    end

    # Reads and returns line from the file.
    # @return [String]
    def read_next_line
      stream.readline
    end

    # Returns if stream is at end of the file.
    # @return [Boolean]
    def eof?
      stream.eof?
    end
  end
end
