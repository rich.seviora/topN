require 'bigdecimal'

module TopN
  # The ValueManager class is responsible for retaining the top _n_ values provided to it,
  # and outputting them on demand.
  class ValueManager

    # @return [Integer]
    attr_reader :number_limit

    # @param [Integer] number_limit - Count of top values to retain. Must be at least 1. Fractions/decimals are truncated.
    # @param [:decimal, :integer] number_mode - The type of parsing to perform on the string.
    def initialize(number_limit, number_mode = :integer)
      raise ArgumentError 'Invalid value' unless number_limit.to_i >= 1
      @number_limit = number_limit
      @number_mode  = number_mode
      @value_store  = []
    end

    # Handles a new value and stores it if it is greater than the existing top N values.
    # @param [String] new_value - The new value to add.
    def process_value(new_value)
      new_value = convert_value(new_value)
      insert_value(new_value)
    end


    # Returns the top values.
    # @return [Array<Numeric>] Returns a copy of the top values.
    def top_values
      value_store
    end

    private

    # @return [Array<Numeric>]
    attr_reader :value_store

    # @return [:decimal, :integer]
    attr_reader :number_mode

    # @param [String] input_line - The input string to be converted.
    # @return [Array<Numeric>]
    def convert_value(input_line)
      number_mode == :decimal ? BigDecimal.new(input_line) : input_line.to_i
    end

    # Inserts the new value if possible.
    # @param [Numeric] new_value - New value to attempt inserting.
    def insert_value(new_value)
      current_values = value_store
      if current_values.empty?
        # First entry should be just appended.
        current_values << new_value
      elsif new_value > current_values.first
        # If greater than current max value, insert into first position.
        current_values.unshift(new_value)
      elsif new_value > current_values.last
        # If less than current smallest value, find location.
        insert_index = get_insert_index new_value
        current_values.insert(insert_index, new_value)
      elsif current_values.length < number_limit
        # Append value less than bounds only if number limit has not been reached.
        current_values << new_value
      end
      current_values.pop if current_values.length > number_limit
    end

    # Gets the index to insert the value at.
    # @param [Numeric] new_value - The new value to insert.
    def get_insert_index(new_value)
      value_store.bsearch_index { |v| new_value > v }
    end
  end
end
