require_relative 'top_n/version'
require_relative 'top_n/processor'
require_relative 'top_n/line_reader'
require_relative 'top_n/value_manager'
require_relative 'top_n/file_generator'

# The TopN module contains logic allowing users to read a file containing numbers on each line and
# output the top _n_ values.
module TopN

end

